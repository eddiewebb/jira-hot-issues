
# Hot Issues for JIRA Service Desk

Hot Issues is a plugin that will display a dialog prompt to users if any known issues relate to the portal or request form they are vieiwing.



### Setup
Use the REST API (UI coming later) to POST & PUT the rules used.

The rules specify the portal ID and 1 JQL to use for top-level/wide spread errors as well as any number of request level JQLs to use for specific forms.

Only one rule per project/Service Desk
```{ 
    "projectId":"1000",
    "jql":"project = PB and labels = hot",
    "requestForms":{ 
        "4":{
            "requestFormId":"4",
            "jql":"project = PB and labels = hot and component = Bamboo Support"
            }
    }
}```

#### Example using Curl
`curl -k -X POST "https://mysite.com/issues/rest/hot-issues/1.0/" -u user:user -d '{"projectId":"1000","jql":"project = PB and labels = hot","requestForms":{ "4":{"requestFormId":"4","jql":"project = PB and labels = hot and component = Bamboo Support"}}}' -H "Content-Type: application/json"`






## Contributing to Atlassian Plugins

Here are the SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
