package com.edwardawebb.jira.hotissues.ao;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.OneToMany;
import net.java.ao.Preload;
import net.java.ao.Searchable;
import net.java.ao.schema.Table;
import net.java.ao.schema.Unique;


@Table("HIPRTJQL")
@Preload
public interface PortalJql extends Entity {

    @Accessor("PROJECTID")
    @Searchable
    @Unique
    long getProjectId();
    @Mutator("PROJECTID")
    void setProjectId(long projectId);

    
    @Accessor("PORTALID")
    public int getPortalId() ;
    @Mutator("PORTALID")
    public void setPortalId(int portalId) ;
    
    @Accessor("JQL")
    public String getPortalJql() ;
    @Mutator("JQL")
    public void setPortalJql(String portalJql);
    
    @OneToMany(reverse="getPortal")
    public RequestFormJql[] getRequestFormJqls();

    
    
}