package com.edwardawebb.jira.hotissues.ao;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.Preload;
import net.java.ao.Searchable;
import net.java.ao.schema.Table;



@Table("HIRQFJQL")
@Preload
public interface RequestFormJql extends Entity {


    @Accessor("PORTAL")
    @Searchable
    public PortalJql getPortal();
    @Mutator("PORTAL")
    public void setPortal(PortalJql portalJql);
    
    
    
    @Accessor("REQFRM")
    public int getRequestFormId();
    @Mutator("REQFRM")
    public void setRequestFormId(int requestFormId) ;
    
    @Accessor("JQL")
    public String getRequestFormJql() ;
    @Mutator("JQL")
    public void setRequestFormJql(String requestFormJql);
     
     

}