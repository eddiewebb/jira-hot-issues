package com.edwardawebb.jira.hotissues.ao.service;

import com.edwardawebb.jira.hotissues.ao.PortalJql;
import com.edwardawebb.jira.hotissues.resources.HotIssuePortalJqlResource;

public interface PortalJqlService {

    

    PortalJql createProjectRules(HotIssuePortalJqlResource resource);

    PortalJql updatePortalJql(long jiraId, HotIssuePortalJqlResource resource);

    PortalJql getPortalJql(long jiraProjectId);

    PortalJql getJqlByPortalId(int portalId);
}
