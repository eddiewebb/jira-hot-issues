package com.edwardawebb.jira.hotissues.ao.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.java.ao.DBParam;
import net.java.ao.Query;

import org.apache.log4j.Logger;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.edwardawebb.jira.hotissues.ao.PortalJql;
import com.edwardawebb.jira.hotissues.ao.RequestFormJql;
import com.edwardawebb.jira.hotissues.resources.HotIssuePortalJqlResource;
import com.edwardawebb.jira.hotissues.resources.HotIssueRequestFormJql;

public class PortalJqlServiceDefaultImpl implements PortalJqlService {

    private final Logger logger = Logger.getLogger(PortalJqlServiceDefaultImpl.class);
    private ActiveObjects ao;

    public PortalJqlServiceDefaultImpl(ActiveObjects activeObjects) {
        this.ao = activeObjects;
    }
    


    @Override
    public PortalJql createProjectRules(HotIssuePortalJqlResource hotIssueJqlResource) {
        PortalJql existingRule = findExistingPortalJqlByProjectId(hotIssueJqlResource.getProjectId());
        
        if( null != existingRule ){
            throw new RuntimeException("JQL for SD Portal is already associated with project this project. Updates must use PUT operations ");
        }
        
        PortalJql result = createPortalJql(hotIssueJqlResource.getProjectId(),
                    hotIssueJqlResource.getPortalId(), 
                    hotIssueJqlResource.getPortalJql(), 
                    hotIssueJqlResource.getRequestFormJqls());
        
        return ao.get(PortalJql.class, result.getID());
    }


    private PortalJql findExistingPortalJqlByProjectId(long projectId) {
        PortalJql[] existingRules = ao.find(PortalJql.class,Query.select().where("PROJECTID = ?", projectId));
        if(existingRules.length == 1 ){
            return existingRules[0];
        }else{
            return null;
        }
    }



    private PortalJql createPortalJql(long jiraId, int portalId, String portalJql, Map<Integer,HotIssueRequestFormJql> requestFormJqls) {
        //save parent object
        PortalJql result = ao.create(PortalJql.class,
                new DBParam("PORTALID", portalId),
                new DBParam("PROJECTID", jiraId), 
                new DBParam("JQL", portalJql));
        
        //create and attach any children        
        List<RequestFormJql> requestForms = new ArrayList<RequestFormJql>();
        for (HotIssueRequestFormJql requestFormResource : requestFormJqls.values()) {
           requestForms.add( createRequestFormJql(result,requestFormResource));
        }
        
        return result;        
    }

    private RequestFormJql createRequestFormJql(PortalJql portal, HotIssueRequestFormJql requestFormResource) {
        RequestFormJql result = ao.create(RequestFormJql.class,
                new DBParam("REQFRM", requestFormResource.getRequestFormId()),
                new DBParam("JQL", requestFormResource.getJql()));
        result.setPortal(portal);
        result.save();
        return result;
    }

    @Override
    public PortalJql updatePortalJql(long jiraId, HotIssuePortalJqlResource resource) {
        PortalJql existingRule = findExistingPortalJqlByProjectId(jiraId);
        
        //use payload data to update ao object
        resource.update(existingRule);
        existingRule.save();
        
        //update any nested objects (delete all existing, insert those provided)
        ao.deleteWithSQL(RequestFormJql.class, "PORTALID = ?", existingRule.getID());
        for (HotIssueRequestFormJql requestFormJql : resource.getRequestFormJqls().values()) {
            createRequestFormJql(existingRule,requestFormJql);
        }
        return ao.get(PortalJql.class, existingRule.getID());
        
    }



    @Override
    public PortalJql getPortalJql(long jiraProjectId) {
       return findExistingPortalJqlByProjectId(jiraProjectId);
    }



    @Override
    public PortalJql getJqlByPortalId(int portalId) {
        PortalJql[] existingRules = ao.find(PortalJql.class,Query.select().where("PORTALID = ?", portalId));
        if(existingRules.length == 1 ){
            return existingRules[0];
        }else{
            return null;
        }
    }

   


}
