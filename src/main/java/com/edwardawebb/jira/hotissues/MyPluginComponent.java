package com.edwardawebb.jira.hotissues;

public interface MyPluginComponent
{
    String getName();
}