/**
 * 
 */
package com.edwardawebb.jira.hotissues.config;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.plugin.projectpanel.impl.AbstractProjectTabPanel;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.browse.BrowseContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
/**
 * This class just provides the front end initial UI. All heavy lifting is done
 * by our custom servlet, invokes via AJS.
 * 
 */
public class ServiceDeskHotIssuesProjectPanelTab extends AbstractProjectTabPanel {
    private static final Logger LOG = LoggerFactory.getLogger(ServiceDeskHotIssuesProjectPanelTab.class);

    public final static String ADMIN_ROLE = "Service Desk Team";

    private final JiraAuthenticationContext authenticationContext;
    private final ProjectRoleManager projectRoleManager;
    


    public ServiceDeskHotIssuesProjectPanelTab(ProjectRoleManager projectRoleManager,
            JiraAuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
        this.projectRoleManager = projectRoleManager;
    }

    @Override
    /**
     * only show to admins
     */
    public boolean showPanel(BrowseContext browseContext) {
        ApplicationUser user = authenticationContext.getUser();
        Project project = browseContext.getProject();
        ProjectRole projectRole = projectRoleManager.getProjectRole(ADMIN_ROLE);
        return projectRoleManager.isUserInProjectRole(user, projectRole, project);
    }

    @Override
    protected Map<String, Object> createVelocityParams(BrowseContext ctx) {
        Map<String, Object> params = super.createVelocityParams(ctx);
        Project project = ctx.getProject();
        params.put("project", project);
        return params;
    }

    @Override
    public String getHtml(BrowseContext ctx) {
        
        return descriptor.getHtml("view", createVelocityParams(ctx));
    }





}
