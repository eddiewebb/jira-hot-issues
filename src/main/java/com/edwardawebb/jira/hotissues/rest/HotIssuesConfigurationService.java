/**
 * 
 */
package com.edwardawebb.jira.hotissues.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.bc.EntityNotFoundException;
import com.edwardawebb.jira.hotissues.ao.PortalJql;
import com.edwardawebb.jira.hotissues.ao.service.PortalJqlService;
import com.edwardawebb.jira.hotissues.resources.HotIssuePortalJqlResource;

/**
 * BEcause the Project Tab Panel is static, we use asynchrnous scripts to interact with this REST service.
 *
 */
@Path("/")
public class HotIssuesConfigurationService {
    private static final Logger LOG = LoggerFactory.getLogger(HotIssuesConfigurationService.class);

    private final PortalJqlService portalJqlService;


    public HotIssuesConfigurationService(PortalJqlService portalJqlService) {
        this.portalJqlService = portalJqlService;
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    public Response createPortalJql(HotIssuePortalJqlResource resource) throws EntityNotFoundException{
        PortalJql portalJqlObj = portalJqlService.createProjectRules(resource);
        return Response.ok(HotIssuePortalJqlResource.from(portalJqlObj)).build();
    }

   
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}/")
    public Response updateProjectJql(@PathParam("id") long jiraId,HotIssuePortalJqlResource resource){
        
        PortalJql portalJqlObj = portalJqlService.updatePortalJql(jiraId, resource);
        return Response.ok(HotIssuePortalJqlResource.from(portalJqlObj)).build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}/")
    public Response getProjectJql(@PathParam("id") long jiraProjectId){
        
        PortalJql portalJqlObj =  portalJqlService.getPortalJql(jiraProjectId);
        
        return Response.ok(HotIssuePortalJqlResource.from(portalJqlObj)).build();
        
    }
    
    
    /*
     * Lookup by portal
     */
    

    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/portal/{id}/")
    public Response getPortalJql(@PathParam("id") int portalId){
        
        PortalJql portalJqlObj =  portalJqlService.getJqlByPortalId(portalId);
        
        return Response.ok(HotIssuePortalJqlResource.from(portalJqlObj)).build();
        
    }
}
