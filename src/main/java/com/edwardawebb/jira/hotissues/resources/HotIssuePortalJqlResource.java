package com.edwardawebb.jira.hotissues.resources;

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.edwardawebb.jira.hotissues.ao.PortalJql;


@XmlRootElement(name = "portal")
@XmlAccessorType(XmlAccessType.FIELD)
public class HotIssuePortalJqlResource {
    


    @XmlElement(required = true, nillable = false)
    private int portalId; 
    @XmlElement(required = true, nillable = false)
    private long projectId;
    @XmlElement
    private String jql;
    @XmlElement(name="requestForms",type=HotIssueRequestFormJql.class)
    private Map<Integer,HotIssueRequestFormJql> requestFormJqls;
    
  

    public static HotIssuePortalJqlResource from(PortalJql portalJqlObj) {
        HotIssuePortalJqlResource resource = new HotIssuePortalJqlResource();
        resource.jql = portalJqlObj.getPortalJql();
        resource.portalId = portalJqlObj.getPortalId();
        resource.projectId = portalJqlObj.getProjectId();
        resource.requestFormJqls = HotIssueRequestFormJql.fromArray(portalJqlObj.getRequestFormJqls());
        return resource;
    }


    public int getPortalId() {
        return portalId;
    }


    public void setPortalId(int portalId) {
        this.portalId = portalId;
    }


    public String getPortalJql() {
        return jql;
    }


    public void setPortalJql(String portalJql) {
        this.jql = portalJql;
    }




    public long getProjectId() {
        return projectId;
    }


    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }





    public String getJql() {
        return jql;
    }


    public void setJql(String jql) {
        this.jql = jql;
    }


    public Map<Integer, HotIssueRequestFormJql> getRequestFormJqls() {
        return requestFormJqls;
    }


    public void setRequestFormJqls(Map<Integer, HotIssueRequestFormJql> requestFormJqls) {
        this.requestFormJqls = requestFormJqls;
    }
    
    
    
    

    /**
     * Updates top level fields, does not update children
     * @param existingRule
     */
    public void update(PortalJql existingRule) {
        existingRule.setPortalId(portalId);
        existingRule.setPortalJql(jql);
        //DONT UPDATE PROJECT ID !!
        
    }


    
    
    

}
