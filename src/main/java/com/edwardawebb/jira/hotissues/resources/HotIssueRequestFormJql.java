package com.edwardawebb.jira.hotissues.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.edwardawebb.jira.hotissues.ao.RequestFormJql;

@XmlRootElement(name = "requestForm")
@XmlAccessorType(XmlAccessType.FIELD)
public class HotIssueRequestFormJql {
    
    @XmlElement
    private int requestFormId;
    @XmlElement
    private String jql;
    
    static List<HotIssueRequestFormJql> from(RequestFormJql[] requestFormJqls){
        List<HotIssueRequestFormJql> resources = new ArrayList<HotIssueRequestFormJql>();
        for (RequestFormJql requestFormJql : requestFormJqls) {
            resources.add(from(requestFormJql));
        }
        return resources;
    }
    static Map<Integer,HotIssueRequestFormJql> fromArray(RequestFormJql[] requestFormJqls){
        Map<Integer,HotIssueRequestFormJql> resources = new HashMap<Integer,HotIssueRequestFormJql>();
        for (RequestFormJql requestFormJql : requestFormJqls) {
            resources.put(requestFormJql.getRequestFormId(),from(requestFormJql));
        }
        return resources;
    }
    
    static HotIssueRequestFormJql from(RequestFormJql requestFormJql){
        HotIssueRequestFormJql resource = new HotIssueRequestFormJql();
        resource.jql = requestFormJql.getRequestFormJql();
        resource.requestFormId = requestFormJql.getRequestFormId();
        return resource;
    }
    
    
    public int getRequestFormId() {
        return requestFormId;
    }
    public void setRequestFormId(int requestFormId) {
        this.requestFormId = requestFormId;
    }
    public String getJql() {
        return jql;
    }
    public void setJql(String jql) {
        this.jql = jql;
    }
    
    

}
