package ut.com.edwardawebb.jira.hotissues;

import org.junit.Test;
import com.edwardawebb.jira.hotissues.MyPluginComponent;
import com.edwardawebb.jira.hotissues.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}